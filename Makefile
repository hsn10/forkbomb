CFLAGS?=	-Wall -ggdb -W -O
CC?=		gcc
LIBS?=
LDFLAGS?=
PREFIX?=	/usr/local
VERSION=1.4
TMPDIR=/tmp/forkbomb-$(VERSION)

all:   forkbomb tags

tags:  *.c
	-ctags *.c

install: forkbomb
	install -s forkbomb $(DESTDIR)$(PREFIX)/sbin
	install -m 444 forkbomb.8 $(DESTDIR)$(PREFIX)/man/man8

forkbomb: forkbomb.c Makefile
	${CC} $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -o forkbomb forkbomb.c $(LIBS)

clean:
	rm -f *.o forkbomb *~ *core tags

tar:   clean
	rm -rf ${TMPDIR}
	install -d $(TMPDIR)
	cp -p Makefile forkbomb.c forkbomb.8 results.txt $(TMPDIR)
	-cd $(TMPDIR) && cd .. && tar cozf forkbomb-$(VERSION).tar.gz forkbomb-$(VERSION)
	echo "Created tar distfile at ${TMPDIR}/../forkbomb-${VERSION}.tar.gz"
	rm -rf ${TMPDIR}

.PHONY: clean install all tar
